const _ = require("underscore");
const express = require('express');
const bodyParser = require('body-parser');
const webhookUtilOracle = require('./webhook/webhookUtil.js');

function init(config) {

    var app = express();
    app.use(function (req, res, next) {
        res.header("Access-Control-Allow-Origin", "*");
        res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
        next();
    });

    const port = process.env.PORT || 3040;

    app.listen(port, () => {
        console.log(`listening on port ${ port }`);
    });

    app.use(bodyParser.urlencoded({extended: true}));
    app.use(bodyParser.json());

    var logger = (config ? config.logger : null);
    if (!logger) {
        const log4js = require('log4js');
        logger = log4js.getLogger();
        logger.setLevel('INFO');
        log4js.replaceConsole(logger);
    }

    var metadata = {
        waitForMoreResponsesMs: 500,
        channelSecretKey: 'Baz5VpxmMCgFM2YZFBnMq2NEdxlMdolY',
        channelUrl: 'https://botphx1I0048HB4E4B7bots-mpaasocimt.botmxp.ocp.oraclecloud.com:443/connectors/v1/tenants/idcs-100b89d671b54afca3069fe360e4bad4/listeners/webhook/channels/F1744AD7-92F2-49DF-A18A-600D269D8DCF'
    };

    if (metadata.channelUrl && metadata.channelSecretKey) {
        logger.info('Google Actions - Using Channel:', metadata.channelUrl);
    }

    //Alexa skill uses JSON bodyParser
    app.use(bodyParser.urlencoded({
        extended: true
    }));

    app.post('/message', function (req, res) {

            var userId = 'Oracle Chatbot - GoogleAssistance Integration';
            var smsRequest = req.body.text;

            logger.info(smsRequest + " -----> received");
            logger.info(req.body)

            if (metadata.channelUrl && metadata.channelSecretKey && userId && smsRequest) {
                var additionalProperties = {
                    "userProfile": {
                        "clientType": "google"
                    }
                };
                webhookUtilOracle.messageToBotWithProperties(metadata.channelUrl, metadata.channelSecretKey, userId, smsRequest, additionalProperties, function (err) {
                    if (err) {
                        logger.info("Failed sending message to Oracle Bot");
                        res.set('Content-Type', 'text/xml');
                    } else {
                        logger.info("sending message to Oracle Bot" + smsRequest);
                        res.sendStatus(200);
                    }
                });

            } else {
                _.defer(function () {
                    res.set('Content-Type', 'text/xml');
                });
            }
            return false;
        }
    );

    app.locals.endpoints = [];

    app.locals.endpoints.push({
        name: 'google',
        method: 'POST',
        endpoint: '/message'
    });

    return app;
}

module.exports = {
    init: init
};

"use strict";

var app = require('./app.js');

//set parameters as appropriate
const config = {
    root: __dirname,
    logLevel: 'INFO',
    logger: null,
    basicAuth: null,
    sslOptions: null
};

// Create an express app instance
var express_app = app.init(config);

// Start the server listening..
//express_app.listen(port);
